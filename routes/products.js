const express = require('express');
const router = express.Router();

let Product = require('../models/product');
let User = require('../models/user');

const AuthGaurd = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('danger', 'Not Authorized!');
    res.redirect('/users/login');
  }
};

router.get('/add', AuthGaurd, (req, res) => {
  res.render('new-product', {
    title: 'Add a new product'
  });
});

router.post('/add', (req, res) => {
  req.checkBody('name', 'Title is required').notEmpty();
  req.checkBody('price', 'Price is required').notEmpty();
  req.checkBody('price', 'Price must be a number').isFloat();
  req.checkBody('currency', 'Currency is required').notEmpty();
  req
    .checkBody('currency', 'Must be a valid currency ("USD", "EUR" or "GBP")')
    .isIn(['USD', 'EUR', 'GBP']);
  req.checkBody('description', 'Description is required').notEmpty();

  let errors = req.validationErrors();

  if (errors) {
    res.render('new-product', {
      title: 'Add a new product',
      errors: errors
    });
  } else {
    let product = new Product();
    product.name = req.body.name;
    product.creator = req.user._id;
    product.price = req.body.price;
    product.currency = req.body.currency;
    product.description = req.body.description;

    product.save(err => {
      if (err) {
        console.error(err);
        return;
      } else {
        req.flash('success', "You've added a product");
        res.redirect('/');
      }
    });
  }
});

router.get('/edit/:id', AuthGaurd, (req, res) => {
  Product.findById(req.params.id, (err, product) => {
    if (product.creator != req.user._id) {
      req.flash('danger', 'Not Authorized!');
      return res.redirect('/');
    }
    res.render('edit-product', {
      title: 'Edit Product',
      product: product
    });
  });
});

router.post('/edit/:id', (req, res) => {
  let product = {};
  product.name = req.body.name;
  product.creator = req.user._id;
  product.price = req.body.price;
  product.currency = req.body.currency;
  product.description = req.body.description;

  let query = { _id: req.params.id };

  Product.update(query, product, err => {
    if (err) {
      console.error(err);
      return;
    } else {
      req.flash('success', "You've update a product");
      res.redirect('/');
    }
  });
});

router.delete('/:id', (req, res) => {
  if (!req.user._id) {
    res.status(500).send();
  }

  let query = { _id: req.params.id };

  Product.findById(req.params.id, (err, product) => {
    if (product.creator != req.user._id) {
      res.status(500).send();
    } else {
      Product.remove(query, err => {
        if (err) {
          console.error(err);
        }
        res.send('Success');
      });
    }
  });
});

router.get('/:id', (req, res) => {
  Product.findById(req.params.id, (err, product) => {
    User.findById(product.creator, (err, user) => {
      res.render('product', {
        product: product,
        creator: user.name
      });
    });
  });
});

module.exports = router;
