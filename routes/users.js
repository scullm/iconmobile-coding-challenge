const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

let User = require('../models/user');

router.get('/registration', (req, res) => {
  res.render('registration');
});

router.post('/registration', (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const username = req.body.username;
  const password = req.body.password;

  req.checkBody('name', 'Name is required').notEmpty();
  req.checkBody('email', 'Email is required').notEmpty();
  req.checkBody('email', 'Email is not valid').isEmail();
  req.checkBody('username', 'Username is required').notEmpty();
  req.checkBody('password', 'Password is required').notEmpty();
  req
    .checkBody('confirmed-password', 'Passwords must match')
    .equals(req.body.password);

  let errors = req.validationErrors();

  if (errors) {
    res.render('registration', {
      errors: errors
    });
  } else {
    let newUser = new User({
      name: name,
      email: email,
      username: username,
      password: password
    });

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) {
          console.error(err);
        }
        newUser.password = hash;
        newUser.save(err => {
          if (err) {
            console.error(err);
            return;
          } else {
            req.flash(
              'success',
              'Thank you for registering. Please log in to create products.'
            );
            res.redirect('/users/login');
          }
        });
      });
    });
  }
});

router.get('/login', (req, res) => {
  res.render('login');
});

router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next);
});

router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success', 'You have logged out!');
  res.redirect('/users/login');
});

module.exports = router;
