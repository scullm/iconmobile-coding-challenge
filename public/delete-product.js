$(document).ready(() => {
  $('.delete-product').on('click', e => {
    $target = $(e.target);
    const id = $target.attr('data-id');
    $.ajax({
      type: 'DELETE',
      url: '/products/' + id,
      success: response => {
        alert('Product deleted');
        window.location.href = '/';
      },
      error: err => {
        console.error(err);
      }
    });
  });
});
