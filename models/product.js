let mongoose = require('mongoose');

let productSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  creator: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  currency: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

let Product = (module.exports = mongoose.model('Product', productSchema));
